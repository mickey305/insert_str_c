#define TRUE_FLAG 1
#define FALSE_FLAG 0
#define MAXBUF_NUMBER 128
#define MAXLINE_NUMBER 64
// #define DEBUG


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
// #include <math.h>
//-------------------------------
#include <stdbool.h>
typedef bool FLAG;
//-------------------------------
// #include <memory.h>
// #include <ctype.h>
// #include <sys/time.h>
// #include <unistd.h>
// #include <sys/socket.h>
// // #include <linux/init.h>
// #include <signal.h>
// // #include <linux/soundcard.h>
// #include <stdarg.h>

//////////////////////////////////////////////////////////////////////
// #
// # Makefile
// #
// CC	= gcc
// SHELL	= /bin/sh
// PROGS	= `ls *.c | sed 's/\.c$$//'`
// #CFLAGS	= -O -I/usr/include/gtk-1.2 -I/usr/include/glib-1.2\
// #		-I/usr/lib/glib/include -I/usr/X11R6/include
// #CFLAGS	= -O `gtk-config --cflags --libs`\
// #		`pkg-config --cflags gtk+-2.0`\
// #		`pkg-config --libs gtk+-2.0`
// #CFLAGS	= -O `gtk-config --cflags --libs`
// CFLAGS	= -O -I/usr/X11R6/include
// #LDFLAGS = -L/usr/lib -L/usr/X11R6/lib\
// #		-rdynamic -ldl -lXi\
// #		-lXext -lX11 -lm\
// #		-lpthread -ljpeg -s
// LDFLAGS = -L/usr/lib -L/usr/X11R6/lib -rdynamic -ldl -LX11 -lXext -lm -ljpeg -s
// #LDFLAGS = -L/usr/lib -L/usr/X11R6/lib\
// #		-rdynamic -ldl -LX11 -lXext -lm -ljpeg -lusb -s
// #LDFLAGS = -lX11 -lm -lpthread -ljpeg -s
// #LDFLAGS = -lX11 -lgdk_imlib -lm -s -pthread -ljpeg
// #LDFLAGS = -I/usr/X11R6/include -L/usr/lib -L/usr/X11R6/lib -lX11\
// #		-lXaw -lXmu -lXt -lXext\
// #		-lgtk -lgdk -lgdk_imlib -rdynamic -lgmodule -lglib -ldl -lXi\
// #		 	-lm -s
// #LIBS	= -L. -lXaw -lXmu -lXt -lXext
// #LIBS	= -lX11 -lXaw -lXmu -lXt -lXext
//
//
// LINKER	= gcc
//
// default all:
// 	 $(CC) $(CFLAGS) $@.c -o $@ $(LDFLAGS)
//
// clean	clobber:
// 	rm -f ${PROGS} *.o
// #
// # end of Makefile
// #
//////////////////////////////////////////////////////////////////////

#ifndef Integer
#define Integer int
#endif

#ifndef Double
#define Double double
#endif

#ifndef Character
#define Character char
#endif

#ifndef FLAG
#define FLAG bool
#endif

//---------------------------------
#ifndef TRUE
#define TRUE true
#endif

#ifndef FALSE
#define FALSE false
#endif

typedef struct
{
	Character arg_input[MAXBUF_NUMBER];
	Character conf_input[MAXBUF_NUMBER];
	Character conf_signal[MAXBUF_NUMBER];
	FLAG      conf_flag;

}PARTS;

typedef struct
{
	Integer arg;
	Integer conf;

}LENGTH;





double average(Integer array[], Integer len);

void set_len(Integer arg_len, Integer conf_len, LENGTH *length);
void remove_file(const Character *filename, Integer flag);
Character *myfgets(Character *str, FILE *in);
Character *myfgets2(Character *str, FILE *in);
FLAG strcmp_flag(Character c1[], Character c2[]);


void inputerr();
void argerr();
void fopenerr();
