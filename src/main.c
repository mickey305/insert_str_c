#include "main.h"

int main(int argc, char *argv[]);

int main(int argc, char *argv[]){

	FILE    *fprw, *fpr;
	PARTS   black_box[MAXLINE_NUMBER];
	LENGTH  len;
	Integer i, j, k;




	if(argc <= 2){

		inputerr();

	}else if(argc <= 3){


		if((fprw = fopen(argv[1], "r+b")) == NULL){
			fopenerr();

		}else{
			if((fpr = fopen(argv[2], "r")) == NULL){
				fopenerr();

			}else{


				i = 0;
				j = 0;
				k = 0;
				while(myfgets2(black_box[i].arg_input, fprw) != NULL){i++;}
				while(myfgets2(black_box[j].conf_signal, fpr) != NULL
					&& myfgets2(black_box[k].conf_input, fpr) != NULL){j++;k++;}
				set_len(i, j, &len);


				#ifdef DEBUG
				printf("---------------------------------------------------\n");
				for(i=0;i<len.arg;i++)printf("ag_in[%d] = %s\n",i,black_box[i].arg_input);
				for(i=0;i<len.conf;i++)printf("c_si[%d] = %s\n",i,black_box[i].conf_signal);
				for(i=0;i<len.conf;i++)printf("c_in[%d] = %s\n",i,black_box[i].conf_input);
				for(i=0;i<len.conf;i++)printf("c_flag[%d] = %d\n",i,black_box[i].conf_flag);
				if(strcmp(black_box[2].arg_input, black_box[1].conf_signal) == 0)printf("Maching OK!\n");
				printf("---------------------------------------------------\n");
				#endif


				//// sample
				fseek(fprw, 0L, SEEK_SET);
				j=0;
				while(j<len.arg){
					// printf("%s", black_box[j++].arg_input);
					fputs(black_box[j].arg_input, fprw);
					i=0;
					while(i<len.conf){
						if(strcmp_flag(black_box[j].arg_input, black_box[i].conf_signal))
							// fputs("\n", fprw),
							fputs(black_box[i].conf_input, fprw),
							black_box[i].conf_flag = TRUE;
						i++;
					}
					j++;
				}
				k=0;
				while(k<len.conf){
					if(!black_box[k].conf_flag)
						fputs(black_box[k].conf_signal, fprw),
						fputs(black_box[k].conf_input, fprw),
						fputs("\n", fprw);
					k++;
				}
				// printf("\n");






				fclose(fpr);
			}
			fclose(fprw);
		}


	}else{

		argerr();

	}

	return 0;
}