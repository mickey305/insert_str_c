origin cmd
----------------
[licence]  
MIT

[clone command]

`git clone https://bitbucket.org/mickey305/insert_str_c.git`

[compile]

`gcc -o main.exe main.c subl.c`

[usage]

`./main.exe [edited-filename] [config-filename]`

[e.g.]

	$ cat edited-file.txt
	[mysql]
	12345...
	[mysqld]
	abcde...

	$ cat conf-file.txt
	[mysql]
	default-character-set=utf8
	[mysqld]
	default-character-set=utf8
	[client]
	default-character-set=utf8

	$ ./main.exe edited-file.txt conf-file.txt
	$ cat edited-file.txt
	[mysql]
	default-character-set=utf8
	12345...
	[mysqld]
	default-character-set=utf8
	abcde...
	[client]
	default-character-set=utf8

	$ cat conf-file.txt
	[mysql]
	default-character-set=utf8
	[mysqld]
	default-character-set=utf8
	[client]
	default-character-set=utf8

	$
